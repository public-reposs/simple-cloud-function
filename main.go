package gcestartstop

import (
	"fmt"
	"net/http"

	"github.com/GoogleCloudPlatform/functions-framework-go/functions"
)

func init() {
	functions.HTTP("Main", main)
}

func main(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello Cloud Functions!")
	w.WriteHeader(http.StatusOK)
}
