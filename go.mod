module gitlab.com/public-reposs/simple-cloud-function

go 1.16

require (
	github.com/GoogleCloudPlatform/functions-framework-go v1.5.3
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
)
